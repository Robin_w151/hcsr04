# HCSR04
## Description

This library provides a simple interface for the HC-SR04 ultrasonic ranging module, which is able to measure distances of up to 4 meters, provided that the surface, which reflects the sound waves is flat and large enough. Furthermore, the measurement angle should not be greater than 15 degrees.

## Example

```
#include <mbed.h>
#include <hcsr04.h>

#define BAUD_RATE 115200

Serial serial(USBTX, USBRX, BAUD_RATE);
DigitalOut trigger(D2);
InterruptIn echo(D3);
HCSR04 sensor(trigger, echo);

int main() {
    while(1) {
        float distance = sensor.measure_distance();
        serial.printf("Distance: %4.2fm\n", distance);
        wait_ms(1000);
    }
}
```
