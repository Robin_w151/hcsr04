#ifndef __HCSR04_H
#define __HCSR04_H

#include <mbed.h>
#include <rtos/rtos.h>

#define MIN_DISTANCE 0.02
#define MAX_DISTANCE 4.00

class HCSR04 {

public:

    HCSR04(DigitalOut& trigger, InterruptIn& echo);
    float measure_distance();

private:

    void echo_signal_rise();
    void echo_signal_fall();

    DigitalOut& trigger;
    InterruptIn& echo;
    Timer timer;
    Semaphore sensor_active;
    Semaphore measurement_ready;
};

#endif//__HCSR04_H