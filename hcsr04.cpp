#include <hcsr04.h>

HCSR04::HCSR04(DigitalOut& trigger, InterruptIn& echo): trigger(trigger), echo(echo), sensor_active(1), measurement_ready(0) {
    echo.rise(callback(this, &HCSR04::echo_signal_rise));
    echo.fall(callback(this, &HCSR04::echo_signal_fall));
}

float HCSR04::measure_distance() {
    DeepSleepLock sleep_lock;
    sensor_active.wait();
    echo.enable_irq();
    trigger.write(1);
    wait_us(10);
    trigger.write(0);
    measurement_ready.wait();
    echo.disable_irq();
    sensor_active.release();
    timestamp_t time_us = timer.read_high_resolution_us();
    return max(MIN_DISTANCE, min(MAX_DISTANCE, (float)time_us * 340.0 / 2000000.0));
}

void HCSR04::echo_signal_rise() {
    timer.reset();
    timer.start();
}

void HCSR04::echo_signal_fall() {
    timer.stop();
    measurement_ready.release();
}
